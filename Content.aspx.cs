﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Content : System.Web.UI.Page
{
   
   
    protected void Page_Load(object sender, EventArgs e)
    {
        string ConStr = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OleDbConnection Con = new OleDbConnection(ConStr);
        if ((Request["id"] != ""))
        {
            string str = "Select * From Article where Act_id=" + Request["id"];

            Con = new OleDbConnection(ConStr);
            Con.Open();
            OleDbCommand cmd = new OleDbCommand(str, Con);
            OleDbDataReader dr = cmd.ExecuteReader();
            
          
            while (dr.Read())
            {
                LabelTitle.Text = "<Strong><B>" + dr["ActTitle"].ToString() + "</B></Strong>";
                LabelSummary.Text = dr["Summary"].ToString();
                LabelDate.Text = dr["Datedata"].ToString();
                TextBox1.Text = dr["Message"].ToString();
                LabelTime.Text = dr["Time"].ToString();
                Labelfilename.Text = "<a href='http://localhost:50269/" + dr["PdfURI"].ToString() + "'>"+dr["Pdffilename"]+"</a></span>";
               
            }
            
            dr.Dispose();
            cmd.Dispose();
            Con.Close();
            Con.Dispose();
        }
    }
}