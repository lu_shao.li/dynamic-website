﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Photo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String path = "~/image/photos/imf/";
        String ImgFile = Request.QueryString["file"];
        path += ImgFile;
        Image1.ImageUrl = path;
    }
}