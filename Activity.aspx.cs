﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Activity : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        string ConStr = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OleDbConnection Con = new OleDbConnection(ConStr);
        Session["PdfURI"]= "Pdf/加熱爐課程時間表.pdf";
        try
        {
            if ((Request["id"] != ""))
            {


                Con.Open();
                OleDbCommand cmd = new OleDbCommand("Select * From Activity where Act_Id=" + Request["id"], Con);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Session["PdfURI"] = dr["PdfURI"].ToString();
                    LabelTitle.Text = "<Strong><B>" + dr["ActTitle"].ToString() + "</B></Strong>";
                    Session["LabelTitle"] = dr["ActTitle"].ToString();
                    LabelDate.Text = dr["Datedata"].ToString();
                    LabelActDate.Text = dr["ActDateStart"].ToString();
                    LabelActDate1.Text = dr["ActDateEnd"].ToString();
                    LabelActTime.Text = dr["StartTime"].ToString() + "~";
                    LabelActTime1.Text = dr["EndTime"].ToString();        
                  
                }
                dr.Dispose();
                cmd.Dispose();
                Con.Close();
                Con.Dispose();
            }

        }
        catch (Exception ex) {
            Response.Write(ex.ToString());
        }
    }
}