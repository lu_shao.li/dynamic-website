﻿<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Src="~/WebUserControl/header.ascx" TagName="menu" TagPrefix="uc1" %>
<%@ Register Src="~/WebUserControl/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/WebUserControl/hoarding.ascx" TagName="hoarding" TagPrefix="uc3" %>
<%@ Register Src="~/WebUserControl/About.ascx" TagName="About" TagPrefix="uc4" %>
<%@ Register Src="~/WebUserControl/Information.ascx" TagName="Information" TagPrefix="uc5" %>
<%@ Register Src="~/WebUserControl/Service.ascx" TagName="Service" TagPrefix="uc6" %>
<%@ Register Src="~/WebUserControl/Book.ascx" TagName="Book" TagPrefix="uc7" %>
<%@ Register Src="~/WebUserControl/List.ascx" TagName="List" TagPrefix="uc8" %>
<%@ Register Src="~/WebUserControl/AddMb.ascx" TagName="AddMb" TagPrefix="uc9" %>
<%@ Register Src="~/WebUserControl/AddArcitle.ascx" TagName="AddArcitle" TagPrefix="uc10" %>
<%@ Register Src="~/WebUserControl/AddActivity.ascx" TagName="AddActivity" TagPrefix="uc11" %>
<%@ Register Src="~/WebUserControl/Login.ascx" TagName="Login" TagPrefix="uc12" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中國石油學會TW</title>
    <link href="~/css/StyleSheet.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" src="JavaScript/JavaScript.js"></script>
    <meta name="keywords" content="中國石油學會TW,中國石油學會台灣,中國石油學會,Chinese Petroleum Institute,中國石油學會Chinese Petroleum Institute,CPI,學會,中國石油">
    <meta name="description" content="中國石油學會Chinese Petroleum Institute 係於民國五十一年(1962年) 十月由國內石油專家、學者、教授、政府有關機構及中油公司主持人等共同發起成立，其宗旨在鼓勵石油學術交流，推廣石油產品應用，並提昇石油產業科技水準，以促進國家整體石油工業建設，增益民眾福祉。">
    <meta name="copyright" content="本網頁著作權屬於中國石油學會所
有© copyright 2013 Chinese Petroleum Institute All Rights Reserved ">
    <meta name="robots" content="all ">
    <meta name="distribution" content="global ">
    <meta name="revisit-after" content="30 days ">
    <meta name="author" content="Lyu shao li ">

    <meta name="msvalidate.01" content="0298DEE658BFD18AF423DC3C31BBCCBB" />

    <style type="text/css">
        #d1 {
            margin-bottom: 0px;
        }

        .auto-style3 {
            height: 21px;
            width: 100px;
        }

        </style>

</head>
<body>
    <form id="form1" runat="server">
        <div id="container">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
            <div align="right" style="height: 20px">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" Font-Underline="True" Font-Size="X-Large"></asp:Label>
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Login</asp:LinkButton>
            </div>
            <!-- 檔頭 -->
            <header id="pageheader">
                <uc1:menu ID="header1" runat="server" />
            </header>

            <!-- 內容 -->
            <article itemscope itemtype="http://schema.org/Article">

                <div itemprop="articleBody" style="float: center">

                    
                    &nbsp;&nbsp;<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
                    </asp:UpdatePanel>
&nbsp;<asp:MultiView ID="MultiView1" runat="server">
                        <asp:View ID="View1" runat="server">
                            <div >
                               <header>
                                <h1 style= "color: #000080" #000080">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Italic="True" Font-Underline="True" Text="佈告欄"></asp:Label>
                                </h1>
                               <br />
                               <br />
                               <br />
                            <uc3:hoarding ID="hoarding" runat="server" /></header>
                                </div>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <section>
                            <uc4:About ID="About" runat="server" />
                               </section>
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                             <uc5:Information ID="Information" runat="server" />
                        </asp:View>
                        <asp:View ID="View4" runat="server">
                            <uc6:Service ID="Service" runat="server" />
                        </asp:View>
                        <asp:View ID="View5" runat="server">
                            <uc7:Book ID="Book" runat="server" />
                        </asp:View>
                        <asp:View ID="View6" runat="server">
                            <uc8:List ID="List" runat="server" />
                        </asp:View>
                        <asp:View ID="View7" runat="server">
                            <uc9:AddMb ID="AddMb" runat="server" />
                        </asp:View>
                        <asp:View ID="View8" runat="server">
                            <uc10:AddArcitle ID="AddArcitle" runat="server" /> 
                        </asp:View>
                        <asp:View ID="View9" runat="server">
                            <uc11:AddActivity ID="AddActivity" runat="server" /> 
                        </asp:View>
                        <asp:View ID="View10" runat="server">
                            <uc12:Login ID="Login" runat="server" /> 
                        </asp:View>
                    </asp:MultiView>
                    <br />

                </div>

            </article>


            <!-- 檔尾 -->
            <footer>
                <uc2:footer ID="footer1" runat="server" />
            </footer>

        </div>
        <div>
        </div>
    </form>
</body>
</html>

