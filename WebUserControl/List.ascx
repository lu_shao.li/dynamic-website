﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="List.ascx.cs" Inherits="WebUserControl_List" %>
<header>
        <div align="center">
            <h1>
                <asp:Image ID="Image1" runat="server" ImageUrl="~/image/number/orderedList1.png" />
                <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Size="Larger" Text="學會五十年回顧"></asp:Label>
            </h1>
            <br />
            <br />
            <iframe id="I1" class="youtube-player" frameborder="0" height="385" name="I1" src="http://www.youtube.com/embed/EHQQQoszXUY" type="text/html" width="640"></iframe>
        </div>
    </header>
   
<p>
</p>

    <section>
        <h1>
        <asp:Image ID="Image2" runat="server" ImageUrl="~/image/number/orderedList2.png" />
                <asp:Label ID="Label8" runat="server" Font-Bold="True" ForeColor="Black" Text="石油學會獎章歷屆得獎人及獲選論文" Font-Size="Larger"></asp:Label>
       </h1>
        <p>
        <asp:Label ID="Label3" runat="server" ForeColor="#3366FF" Text="請輸入標題:" Visible="False"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
        <asp:FileUpload ID="FileUpload1" runat="server" Visible="False" />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="上傳" Visible="False" />
        <asp:Label ID="Label2" runat="server" ForeColor="#3366FF" Text="上傳最新菁英榜" Visible="False"></asp:Label>
        <asp:Label ID="Label4" runat="server" ForeColor="Red" Visible="False"></asp:Label>
            </p>
    <p>
        <asp:Label ID="Label6" runat="server" ForeColor="Red" Text="勾選刪除項目:" Visible="False"></asp:Label>
            </p>
    <p>
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="AccessDataSource1" DataTextField="Title" DataValueField="L_ID" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" BackColor="#FFFFCC" Font-Bold="True" Font-Size="Large" ForeColor="#003399">
        </asp:DropDownList>
        <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/db_ADO.mdb" SelectCommand="SELECT L_ID, Title, URL FROM List ORDER BY L_ID" ></asp:AccessDataSource>
            </p>
        <p>
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" DataSourceID="AccessDataSource1" DataTextField="Title" DataValueField="L_ID" OnSelectedIndexChanged="CheckBoxList1_SelectedIndexChanged1" Visible="False">
            </asp:CheckBoxList>
            </p>
        <p>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="刪除" Visible="False" />
            </p>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
            <ContentTemplate>
                <object border="0" classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" height="700" width="860">
                    <param name="_Version" value="65539" />
                    <param name="_ExtentX" value="20108" />
                    <param name="_ExtentY" value="10866" />
                    <param name="_StockProps" value="0" />
                    <param name="SRC"value="<%= Session["list"].ToString()%>" />
                    <object data='<%= Session["list"].ToString()%>' height="700" type="application/pdf" width="860">
                    </object>
                </object>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="DropDownList1">
                </asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        </br>

                </object>
           