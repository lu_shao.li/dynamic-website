﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Information.ascx.cs" Inherits="WebUserControl_Information" %>
<div>
<header style="margin-right: 35px">
        <asp:Label ID="Label1" runat="server" Text="活動資訊" Font-Bold="True"></asp:Label>
                <header id="postheader0" >
                    <b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-size:14.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:
ZH-TW;mso-bidi-language:AR-SA">(</span><span style="font-size:14.0pt;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:
minor-bidi;color:red;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;
mso-bidi-language:AR-SA">※</span><span style="font-size:14.0pt;font-family:
&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;
color:red;mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-bidi-language:
AR-SA">最新</span><span style="font-size:14.0pt;font-family:&quot;新細明體&quot;,&quot;serif&quot;;
mso-ascii-theme-font:minor-fareast;mso-fareast-theme-font:minor-fareast;
mso-hansi-theme-font:minor-fareast;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-bidi-theme-font:minor-bidi;color:red;mso-ansi-language:EN-US;mso-fareast-language:
ZH-TW;mso-bidi-language:AR-SA">！</span><span lang="EN-US" style="font-size:14.0pt;
font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;
mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-bidi-language:AR-SA">)</span><span style="font-size:14.0pt;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;
mso-fareast-language:ZH-TW;mso-bidi-language:AR-SA">辦理中</span></b><span lang="EN-US" style="font-size:14.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:
ZH-TW;mso-bidi-language:AR-SA">---<br />
    <div>
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Act_Id" DataSourceID="AccessDataSource1" EmptyDataText="沒有資料錄可顯示。" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:BoundField DataField="Datedata" HeaderText="發表日期" SortExpression="Datedata" />
                            <asp:HyperLinkField DataNavigateUrlFields="Act_Id" DataNavigateUrlFormatString="~/Activity.aspx?id={0}" DataTextField="ActTitle" HeaderText="活動名稱" />
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
        </div>
                    <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data\db_ADO.mdb" DeleteCommand="DELETE FROM `Activity` WHERE `Act_Id` = ?" InsertCommand="INSERT INTO `Activity` (`Act_Id`, `ActTitle`, `ActContent`, `ActAuthor`, `ActDateStart`, `ActDateEnd`, `Datedata`, `StartTime`, `EndTime`, `PdfURI`, `isPass`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" SelectCommand="SELECT `Act_Id`, `ActTitle`, `ActContent`, `ActAuthor`, `ActDateStart`, `ActDateEnd`, `Datedata`, `StartTime`, `EndTime`, `PdfURI`, `isPass` FROM `Activity` where isPass='0' ORDER BY [Act_Id] DESC " UpdateCommand="UPDATE `Activity` SET `ActTitle` = ?, `ActContent` = ?, `ActAuthor` = ?, `ActDateStart` = ?, `ActDateEnd` = ?, `Datedata` = ?, `StartTime` = ?, `EndTime` = ?, `PdfURI` = ?, `isPass` = ? WHERE `Act_Id` = ?">
                        <DeleteParameters>
                            <asp:Parameter Name="Act_Id" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:Parameter Name="Act_Id" Type="Int32" />
                            <asp:Parameter Name="ActTitle" Type="String" />
                            <asp:Parameter Name="ActContent" Type="String" />
                            <asp:Parameter Name="ActAuthor" Type="String" />
                            <asp:Parameter Name="ActDateStart" Type="String" />
                            <asp:Parameter Name="ActDateEnd" Type="String" />
                            <asp:Parameter Name="Datedata" Type="String" />
                            <asp:Parameter Name="StartTime" Type="String" />
                            <asp:Parameter Name="EndTime" Type="String" />
                            <asp:Parameter Name="PdfURI" Type="String" />
                            <asp:Parameter Name="isPass" Type="String" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="ActTitle" Type="String" />
                            <asp:Parameter Name="ActContent" Type="String" />
                            <asp:Parameter Name="ActAuthor" Type="String" />
                            <asp:Parameter Name="ActDateStart" Type="String" />
                            <asp:Parameter Name="ActDateEnd" Type="String" />
                            <asp:Parameter Name="Datedata" Type="String" />
                            <asp:Parameter Name="StartTime" Type="String" />
                            <asp:Parameter Name="EndTime" Type="String" />
                            <asp:Parameter Name="PdfURI" Type="String" />
                            <asp:Parameter Name="isPass" Type="String" />
                            <asp:Parameter Name="Act_Id" Type="Int32" />
                        </UpdateParameters>
                    </asp:AccessDataSource>
                    <br />
                    </span><br />
                    </header>
        <header>
            
            <b style="mso-bidi-font-weight:normal"><span style="font-size:14.0pt;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:
&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;
mso-fareast-language:ZH-TW;mso-bidi-language:AR-SA">已辦理</span></b><span lang="EN-US" style="font-size:14.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:
ZH-TW;mso-bidi-language:AR-SA">---<asp:GridView ID="GridView2" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Act_Id" DataSourceID="AccessDataSource2" EmptyDataText="沒有資料錄可顯示。" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Datedata" HeaderText="發表日期" SortExpression="Datedata" />
                    <asp:HyperLinkField DataNavigateUrlFields="Act_Id" DataNavigateUrlFormatString="~/Activity.aspx?id={0}" DataTextField="ActTitle" HeaderText="標題" />
                </Columns>
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>
            <asp:AccessDataSource ID="AccessDataSource2" runat="server" DataFile="~/App_Data/db_ADO.mdb" DeleteCommand="DELETE FROM `Activity` WHERE `Act_Id` = ?" InsertCommand="INSERT INTO `Activity` (`Act_Id`, `ActTitle`, `ActContent`, `ActAuthor`, `ActDateStart`, `ActDateEnd`, `Datedata`, `StartTime`, `EndTime`, `PdfURI`, `isPass`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" SelectCommand="SELECT `Act_Id`, `ActTitle`, `ActContent`, `ActAuthor`, `ActDateStart`, `ActDateEnd`, `Datedata`, `StartTime`, `EndTime`, `PdfURI`, `isPass` FROM `Activity` where isPass='1' ORDER BY [Act_Id] DESC " UpdateCommand="UPDATE `Activity` SET `ActTitle` = ?, `ActContent` = ?, `ActAuthor` = ?, `ActDateStart` = ?, `ActDateEnd` = ?, `Datedata` = ?, `StartTime` = ?, `EndTime` = ?, `PdfURI` = ?, `isPass` = ? WHERE `Act_Id` = ?">
                <DeleteParameters>
                    <asp:Parameter Name="Act_Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Act_Id" Type="Int32" />
                    <asp:Parameter Name="ActTitle" Type="String" />
                    <asp:Parameter Name="ActContent" Type="String" />
                    <asp:Parameter Name="ActAuthor" Type="String" />
                    <asp:Parameter Name="ActDateStart" Type="String" />
                    <asp:Parameter Name="ActDateEnd" Type="String" />
                    <asp:Parameter Name="Datedata" Type="String" />
                    <asp:Parameter Name="StartTime" Type="String" />
                    <asp:Parameter Name="EndTime" Type="String" />
                    <asp:Parameter Name="PdfURI" Type="String" />
                    <asp:Parameter Name="isPass" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ActTitle" Type="String" />
                    <asp:Parameter Name="ActContent" Type="String" />
                    <asp:Parameter Name="ActAuthor" Type="String" />
                    <asp:Parameter Name="ActDateStart" Type="String" />
                    <asp:Parameter Name="ActDateEnd" Type="String" />
                    <asp:Parameter Name="Datedata" Type="String" />
                    <asp:Parameter Name="StartTime" Type="String" />
                    <asp:Parameter Name="EndTime" Type="String" />
                    <asp:Parameter Name="PdfURI" Type="String" />
                    <asp:Parameter Name="isPass" Type="String" />
                    <asp:Parameter Name="Act_Id" Type="Int32" />
                </UpdateParameters>
            </asp:AccessDataSource>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <span lang="EN-US" style="font-size:14.0pt;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;
mso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:
ZH-TW;mso-bidi-language:AR-SA">
                    
                    <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                    </asp:Timer>
                    </span>
                </ContentTemplate>
            </asp:UpdatePanel>
            <br />
            <br />
            <br />
            </span></header>

    </header>
    </div>
    <header>
        <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Blue" Text="歷年辦理課程"></asp:Label>
        <asp:Label ID="Label13" runat="server" Font-Size="Large" ForeColor="Red" Text="※詳細內容請點下方課程名稱"></asp:Label>
        <br />
        <br />
            <hr/>
        
        <asp:DataList ID="DataList1" runat="server"  RepeatColumns="3" Width="637px">
            <ItemTemplate>
                <asp:Image ID="Image1" runat="server" Height="340px" ImageUrl='<%# Eval("Name", "~/image/photos/imf/{0}") %>' Width="267px" style="margin-right: 0px" />
                <br />
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("Name", "~/Photo.aspx?file={0}") %>' Text='<%# Eval("Name") %>'></asp:HyperLink>
            </ItemTemplate>
        </asp:DataList>
                    <hr/>
        
            
        
            
        <br />
        
            
        
            
        </header>