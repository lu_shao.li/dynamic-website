﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddArcitle.ascx.cs" Inherits="WebUserControl_AddArcitle" %>
<div>

    <br />
    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="Act_Id" DataSourceID="AccessDataSource2" EmptyDataText="沒有資料錄可顯示。" AllowPaging="True" AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowEditButton="True" >
            <ControlStyle Font-Underline="True" ForeColor="Blue" />
            </asp:CommandField>
            <asp:CommandField ShowDeleteButton="True" >
            <ControlStyle Font-Underline="True" ForeColor="Blue" />
            </asp:CommandField>
            <asp:BoundField DataField="ActTitle" HeaderText="標題" SortExpression="ActTitle" />
            <asp:BoundField DataField="Summary" HeaderText="摘要" SortExpression="Summary" />
            <asp:BoundField DataField="Message" HeaderText="內容" SortExpression="Message" />
            <asp:HyperLinkField DataNavigateUrlFields="Act_Id" DataNavigateUrlFormatString="~/UPWebForm.aspx?id={0}" DataTextField="PdfFileName" HeaderText="附檔" Text="上傳" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:AccessDataSource ID="AccessDataSource2" runat="server" DataFile="~/App_Data/db_ADO.mdb" DeleteCommand="DELETE FROM `Article` WHERE `Act_Id` = ?" InsertCommand="INSERT INTO `Article` (`Act_Id`, `ActTitle`, `Message`, `ActAuthor`, `Datedata`, `Time`, `PdfURI`, `Summary`, `PdfFileName`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)" SelectCommand="SELECT `Act_Id`, `ActTitle`, `Message`, `ActAuthor`, `Datedata`, `Time`, `PdfURI`, `Summary`, `PdfFileName` FROM `Article`" UpdateCommand="UPDATE `Article` SET `ActTitle` = ?, `Message` = ?, `ActAuthor` = ?, `Datedata` = ?, `Time` = ?, `PdfURI` = ?, `Summary` = ?, `PdfFileName` = ? WHERE `Act_Id` = ?">
        <DeleteParameters>
            <asp:Parameter Name="Act_Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Act_Id" Type="Int32" />
            <asp:Parameter Name="ActTitle" Type="String" />
            <asp:Parameter Name="Message" Type="String" />
            <asp:Parameter Name="ActAuthor" Type="String" />
            <asp:Parameter Name="Datedata" Type="String" />
            <asp:Parameter Name="Time" Type="String" />
            <asp:Parameter Name="PdfURI" Type="String" />
            <asp:Parameter Name="Summary" Type="String" />
            <asp:Parameter Name="PdfFileName" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ActTitle" Type="String" />
            <asp:Parameter Name="Message" Type="String" />
            <asp:Parameter Name="ActAuthor" Type="String" />
            <asp:Parameter Name="Datedata" Type="String" />
            <asp:Parameter Name="Time" Type="String" />
            <asp:Parameter Name="PdfURI" Type="String" />
            <asp:Parameter Name="Summary" Type="String" />
            <asp:Parameter Name="PdfFileName" Type="String" />
            <asp:Parameter Name="Act_Id" Type="Int32" />
        </UpdateParameters>
    </asp:AccessDataSource>
    <br />
</div>
<div align="center">
         <h1>新增最新消息</h1><br />
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Larger" Text="標題:"></asp:Label>
        <asp:TextBox ID="TextBoxTitle" runat="server" Width="400px"></asp:TextBox>
        <br />
        <hr />
        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Larger" Text="摘要:"></asp:Label>
        <asp:TextBox ID="TextBoxS" runat="server" Width="400px"></asp:TextBox>
        <br />
        <div align="right">
        </div>
        <hr />
        <br />
        <asp:TextBox ID="TextBoxAt" runat="server" Height="244px" TextMode="MultiLine" Width="519px"></asp:TextBox>
         <asp:Label ID="LabelTime" runat="server"></asp:Label>
         <asp:Label ID="LabelDate" runat="server"></asp:Label>
         <asp:Label ID="Label5" runat="server"></asp:Label>
         <asp:Label ID="Label6" runat="server"></asp:Label>
         <br />
        <hr />
        <div align="right">
        作者：<asp:Label ID="LabelAuthor" runat="server"></asp:Label>
            
        </div>
        <hr />
         <asp:Label ID="Label4" runat="server" Text="檔案上傳:"></asp:Label>
         <asp:FileUpload ID="FileUpload1" runat="server" />
         <br />
        <br />
         <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="儲存" />
         <br />
         <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:Label>
         <br />
            <asp:AccessDataSource ID="AccessDataSourceAddActicle" runat="server" DataFile="~/App_Data/db_ADO.mdb" SelectCommand="SELECT * FROM [BookDisplay] WHERE ([Typ] = ?) ORDER BY [B_ID]" InsertCommand="INSERT INTO [Article] ([ActTitle], [Summary], [Datedata], [Time], [Message], [ActAuthor], [PdfURI], [PdfFileName]) VALUES (@Title, @Summary, @Date, @Time, @Message, @Author, @PdfURI, @PdfFileName)" EnableViewState="False">
                <InsertParameters>
                    <asp:ControlParameter ControlID="TextBoxTitle" DefaultValue="" Name="Title" PropertyName="Text" />
                    <asp:ControlParameter ControlID="TextBoxS" Name="Summary" PropertyName="Text" />
                    <asp:ControlParameter ControlID="LabelDate" Name="Date" PropertyName="Text" />
                    <asp:ControlParameter ControlID="LabelTime" Name="Time" PropertyName="Text" />
                    <asp:ControlParameter ControlID="TextBoxAt" Name="Message" PropertyName="Text" />
                    <asp:ControlParameter ControlID="LabelAuthor" Name="ActAuthor" PropertyName="Text" />
                    <asp:ControlParameter ControlID="Label5" Name="PdfURI" PropertyName="Text" />
                    <asp:ControlParameter ControlID="Label6" Name="PdfFileName" PropertyName="Text" />
                </InsertParameters>
                
            </asp:AccessDataSource>
            

         <br />
    </div>
    <p>
        <br />
    </p>
    <p>
    </p>