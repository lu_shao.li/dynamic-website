﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControl_List : System.Web.UI.UserControl
{
    String connString;
    OleDbConnection Con;
    protected void Page_Load(object sender, EventArgs e)
    {
        connString = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            Session["list"] = "Pdf/List/98年得獎人.pdf";
            if (Session["Username"] != null && Convert.ToInt16(Session["Pasw"]) == 1)
            {
                OleDbConnection conn = new OleDbConnection(connString);
                conn.Open();
                OleDbCommand cmd = new OleDbCommand("select * from UserTable Where  Username='" + Session["Username"].ToString() + "'", conn);
                OleDbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr["Rank"].ToString() != "")
                    {
                        Button1.Visible = true;
                        FileUpload1.Visible = true;
                        Label2.Visible = true;
                        Label3.Visible = true;
                        TextBox1.Visible = true;
                        Label4.Visible = true;
                        Label6.Visible = true;
                        Button2.Visible = true;
                        CheckBoxList1.Visible = true;
                    }
                }
                dr.Dispose();
                conn.Close();
                conn.Dispose();
                cmd.Dispose();
            }
            else {
                Button1.Visible = false;
                FileUpload1.Visible = false;
                Label2.Visible = false;
                Label3.Visible = false;
                TextBox1.Visible = false;
                Label4.Visible = false;
                Label6.Visible = false;
                Button2.Visible = false;
                CheckBoxList1.Visible = false;
            }
    }
    protected void Upload(FileUpload myFileUpload)
    {
        Boolean fileAllow = false;
        String[] allowExtensions = { ".pdf" };
        String path = HttpContext.Current.Request.MapPath("~/Pdf/List/");
        if (myFileUpload.HasFile)
        {
            String fileExtension = System.IO.Path.GetExtension(myFileUpload.FileName).ToLower();
            for (int i = 0; i < allowExtensions.Length; i++)
            {
                if (fileExtension == allowExtensions[i])
                {
                    fileAllow = true;
                }

            }
            if (fileAllow)
            {
                try
                {

                    myFileUpload.SaveAs(path + myFileUpload.FileName);
                }
                catch (Exception ex)
                {
                }
            }
            else
            {

            }

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
        Boolean format = false;
        if (fileExtension == ".pdf")
        {
            format = true;
        }
        else
        {
            Label4.Text += "非PDF檔!";
        }
        if (FileUpload1.HasFile)
        {
            if (TextBox1.Text != "")
            {
                if (format)
                {
                    Con = new OleDbConnection(connString);
                    Con.Open();
                    OleDbCommand com = new OleDbCommand("Insert Into List(Title,URL) Values('" + TextBox1.Text + "','" + "Pdf/List/" + FileUpload1.FileName + "')", Con);
                    com.ExecuteNonQuery();
                    Upload(FileUpload1);
                    com.Dispose();
                    Con.Close();
                    Con.Dispose();
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Label4.Text = "標題空白!";
            }
        }
        else
        {
            Label4.Text = "沒有檔案!";
        }
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        OleDbConnection conn = new OleDbConnection(connString);
        conn.Open();
        OleDbCommand com = new OleDbCommand("Select * From List where L_ID=" + DropDownList1.SelectedValue, conn);
        OleDbDataReader dr = com.ExecuteReader();
        while (dr.Read())
        {
            Session["list"] = dr["URL"].ToString();
        }
        conn.Close();
        com.Dispose();
        conn.Dispose();
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Con = new OleDbConnection(connString);
        Con.Open();
        OleDbCommand dcom = new OleDbCommand("Select * From List where L_ID=" + CheckBoxList1.SelectedValue, Con);

        OleDbDataReader drd = dcom.ExecuteReader();
        while (drd.Read())
        {
            Session["LDURL"] = drd["URL"].ToString();
        }
        dcom.Dispose();

        OleDbCommand com = new OleDbCommand("Delete From List Where L_ID=" + CheckBoxList1.SelectedValue, Con);
        com.ExecuteNonQuery();
        Con.Close();
        com.Dispose();
        Con.Dispose();
       
        System.IO.FileInfo fi = new System.IO.FileInfo(Server.MapPath(Session["LDURL"].ToString()));
        try
        {
            fi.Delete();
        }
        catch (System.IO.IOException ex)
        {

        }

        Response.Redirect("Default.aspx");
    }
    protected void CheckBoxList1_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }
    
}