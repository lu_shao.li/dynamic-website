﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddActivity.ascx.cs" Inherits="WebUserControl_AddActivity" %>
<div>

    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Act_Id" DataSourceID="AccessDataSource1" EmptyDataText="沒有資料錄可顯示。" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="ActTitle" HeaderText="活動標題" SortExpression="ActTitle" />
            <asp:BoundField DataField="ActDateStart" HeaderText="開始日期" SortExpression="ActDateStart" />
            <asp:BoundField DataField="ActDateEnd" HeaderText="結束日期" SortExpression="ActDateEnd" />
            <asp:BoundField DataField="StartTime" HeaderText="開始時間" SortExpression="StartTime" />
            <asp:BoundField DataField="EndTime" HeaderText="結束時間" SortExpression="EndTime" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>
    <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/db_ADO.mdb" DeleteCommand="DELETE FROM `Activity` WHERE `Act_Id` = ?" InsertCommand="INSERT INTO `Activity` (`Act_Id`, `ActTitle`, `ActContent`, `ActAuthor`, `ActDateStart`, `ActDateEnd`, `Datedata`, `StartTime`, `EndTime`, `PdfURI`, `isPass`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" SelectCommand="SELECT `Act_Id`, `ActTitle`, `ActContent`, `ActAuthor`, `ActDateStart`, `ActDateEnd`, `Datedata`, `StartTime`, `EndTime`, `PdfURI`, `isPass` FROM `Activity` ORDER BY [Acｔ_id] DESC" UpdateCommand="UPDATE `Activity` SET `ActTitle` = ?, `ActContent` = ?, `ActAuthor` = ?, `ActDateStart` = ?, `ActDateEnd` = ?, `Datedata` = ?, `StartTime` = ?, `EndTime` = ?, `PdfURI` = ?, `isPass` = ? WHERE `Act_Id` = ?">
        <DeleteParameters>
            <asp:Parameter Name="Act_Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Act_Id" Type="Int32" />
            <asp:Parameter Name="ActTitle" Type="String" />
            <asp:Parameter Name="ActContent" Type="String" />
            <asp:Parameter Name="ActAuthor" Type="String" />
            <asp:Parameter Name="ActDateStart" Type="String" />
            <asp:Parameter Name="ActDateEnd" Type="String" />
            <asp:Parameter Name="Datedata" Type="String" />
            <asp:Parameter Name="StartTime" Type="String" />
            <asp:Parameter Name="EndTime" Type="String" />
            <asp:Parameter Name="PdfURI" Type="String" />
            <asp:Parameter Name="isPass" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="ActTitle" Type="String" />
            <asp:Parameter Name="ActContent" Type="String" />
            <asp:Parameter Name="ActAuthor" Type="String" />
            <asp:Parameter Name="ActDateStart" Type="String" />
            <asp:Parameter Name="ActDateEnd" Type="String" />
            <asp:Parameter Name="Datedata" Type="String" />
            <asp:Parameter Name="StartTime" Type="String" />
            <asp:Parameter Name="EndTime" Type="String" />
            <asp:Parameter Name="PdfURI" Type="String" />
            <asp:Parameter Name="isPass" Type="String" />
            <asp:Parameter Name="Act_Id" Type="Int32" />
        </UpdateParameters>
    </asp:AccessDataSource>
    <br/><br/><br/>
</div>
<div align="center">
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Larger" Text="標題:"></asp:Label>
        <asp:TextBox ID="TextBoxTitle" runat="server" Width="400px"></asp:TextBox>
        <br />
        <hr />
         <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
             <ContentTemplate>
                 <asp:Label ID="Label9" runat="server" Text="活動時間:"></asp:Label>
                 <asp:DropDownList ID="DropDownList1" runat="server" style="margin-top: 0px">
                     <asp:ListItem>0</asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                 </asp:DropDownList>
                 <asp:Label ID="Label10" runat="server" Text=":"></asp:Label>
                 <asp:DropDownList ID="DropDownList5" runat="server">
                     <asp:ListItem>0</asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                     <asp:ListItem>32</asp:ListItem>
                     <asp:ListItem>33</asp:ListItem>
                     <asp:ListItem>34</asp:ListItem>
                     <asp:ListItem>35</asp:ListItem>
                     <asp:ListItem>36</asp:ListItem>
                     <asp:ListItem>37</asp:ListItem>
                     <asp:ListItem>38</asp:ListItem>
                     <asp:ListItem>39</asp:ListItem>
                     <asp:ListItem>40</asp:ListItem>
                     <asp:ListItem>41</asp:ListItem>
                     <asp:ListItem>42</asp:ListItem>
                     <asp:ListItem>43</asp:ListItem>
                     <asp:ListItem>44</asp:ListItem>
                     <asp:ListItem>45</asp:ListItem>
                     <asp:ListItem>46</asp:ListItem>
                     <asp:ListItem>47</asp:ListItem>
                     <asp:ListItem>48</asp:ListItem>
                     <asp:ListItem>49</asp:ListItem>
                     <asp:ListItem>50</asp:ListItem>
                     <asp:ListItem>51</asp:ListItem>
                     <asp:ListItem>52</asp:ListItem>
                     <asp:ListItem>53</asp:ListItem>
                     <asp:ListItem>54</asp:ListItem>
                     <asp:ListItem>55</asp:ListItem>
                     <asp:ListItem>56</asp:ListItem>
                     <asp:ListItem>57</asp:ListItem>
                     <asp:ListItem>58</asp:ListItem>
                     <asp:ListItem>59</asp:ListItem>
                 </asp:DropDownList>
                 <asp:Label ID="Label11" runat="server" Text="~"></asp:Label>
                 <asp:DropDownList ID="DropDownList2" runat="server">
                     <asp:ListItem>0</asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                 </asp:DropDownList>
                 :<asp:DropDownList ID="DropDownList6" runat="server">
                     <asp:ListItem>0</asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                     <asp:ListItem>32</asp:ListItem>
                     <asp:ListItem>33</asp:ListItem>
                     <asp:ListItem>34</asp:ListItem>
                     <asp:ListItem>35</asp:ListItem>
                     <asp:ListItem>36</asp:ListItem>
                     <asp:ListItem>37</asp:ListItem>
                     <asp:ListItem>38</asp:ListItem>
                     <asp:ListItem>39</asp:ListItem>
                     <asp:ListItem>40</asp:ListItem>
                     <asp:ListItem>41</asp:ListItem>
                     <asp:ListItem>42</asp:ListItem>
                     <asp:ListItem>43</asp:ListItem>
                     <asp:ListItem>44</asp:ListItem>
                     <asp:ListItem>45</asp:ListItem>
                     <asp:ListItem>46</asp:ListItem>
                     <asp:ListItem>47</asp:ListItem>
                     <asp:ListItem>48</asp:ListItem>
                     <asp:ListItem>49</asp:ListItem>
                     <asp:ListItem>50</asp:ListItem>
                     <asp:ListItem>51</asp:ListItem>
                     <asp:ListItem>52</asp:ListItem>
                     <asp:ListItem>53</asp:ListItem>
                     <asp:ListItem>54</asp:ListItem>
                     <asp:ListItem>55</asp:ListItem>
                     <asp:ListItem>56</asp:ListItem>
                     <asp:ListItem>57</asp:ListItem>
                     <asp:ListItem>58</asp:ListItem>
                     <asp:ListItem>59</asp:ListItem>
                 </asp:DropDownList>
                 <br />
                 <asp:Label ID="Label6" runat="server" Text="日期:"></asp:Label>
                 <asp:Button ID="Button2" runat="server" Font-Size="Smaller" ForeColor="#3399FF" OnClick="Button2_Click" Text="選取開始日期" />
                 :<asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged" ReadOnly="True" Width="89px"></asp:TextBox>
                 <asp:Label ID="Label7" runat="server" Text="~"></asp:Label>
                 <asp:Button ID="Button3" runat="server" Font-Size="Smaller" ForeColor="#3399FF" OnClick="Button3_Click" style="margin-top: 0px" Text="選取結束日期" />
                 :<asp:TextBox ID="TextBox2" runat="server" ReadOnly="True" Width="89px"></asp:TextBox>
                 <br />
                 <asp:MultiView ID="MultiView2" runat="server">
                     <asp:View ID="View3" runat="server">
                         <asp:Calendar ID="Calendar2" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" OnSelectionChanged="Calendar2_SelectionChanged" Width="220px">
                             <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                             <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                             <OtherMonthDayStyle ForeColor="#999999" />
                             <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                             <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                             <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                             <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                             <WeekendDayStyle BackColor="#CCCCFF" />
                         </asp:Calendar>
                     </asp:View>
                     <asp:View ID="View4" runat="server">
                         <asp:Calendar ID="Calendar4" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" OnSelectionChanged="Calendar4_SelectionChanged" Width="220px">
                             <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
                             <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                             <OtherMonthDayStyle ForeColor="#999999" />
                             <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                             <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                             <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                             <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                             <WeekendDayStyle BackColor="#CCCCFF" />
                         </asp:Calendar>
                     </asp:View>
                     <br/>
                 </asp:MultiView>
                 <br />
             </ContentTemplate>
             <Triggers>
                 <asp:AsyncPostBackTrigger ControlID="Button2" />
                 <asp:AsyncPostBackTrigger ControlID="Button3" />
                 <asp:AsyncPostBackTrigger ControlID="Calendar2" />
                 <asp:AsyncPostBackTrigger ControlID="Calendar4" />
             </Triggers>
        </asp:UpdatePanel>
        <br />
         <br />
        <br />
        <div align="right">
        </div>
        <hr />
        <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Larger" Text="活動內容上傳:"></asp:Label>
         <br />
         <asp:FileUpload ID="FileUpload1" runat="server" />
         <br>
         <asp:Label ID="LabelTime" runat="server"></asp:Label>
         <asp:Label ID="LabelDate" runat="server"></asp:Label>
         <asp:Label ID="Label5" runat="server"></asp:Label>
         <br />
        <hr />
        <div align="left">
            <br />
            <br />
            
        </div>
         <br />
        <br />
         <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="儲存" />
         <br />
         <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Larger" ForeColor="Red"></asp:Label>
         <br />
    </div>