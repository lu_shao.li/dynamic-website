﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="footer.ascx.cs" Inherits="footer" %>
<small>
    <asp:Label ID="Label6" runat="server" Font-Size="Medium" Text="© - 中國石油學會" Font-Bold="True" ForeColor="#3366CC"></asp:Label>
    <br />
    <!-- 計數器原始碼開始 -->
<script src="http://counter.i2yes.com/i2yesCounter.js" type="text/javascript"></script>
<script type="text/javascript">document.write(i2yesCounter.render({
    y: 'counter',
    p: 'cpi', //改成您專有的計數器名稱,注意別跟別人一樣
    v: 'http://localhost:60424/', //您的網址,不對的話無法使用,也可以用沒有 www 的網址,如 i2yes.com
    d: 6, //數字位數
    r: 1, //1=不接受Reload,0=Reload會+1
    t: 'font159', //字型 font001 - font156 可用
    s: 0,  //指定大小,只能輸入數字例;100, 0為不指定尺寸(原寸)
    n: 0	//指定起始的數字
}));</script>
<!-- 計數器原始碼結束 -->


    <br />


</small>
<small>
    <div align="center">
        <b>
            <asp:Label ID="Label2" runat="server" Text="地址:" ForeColor="#0033CC"></asp:Label>
            <asp:Label ID="Label3" runat="server" Font-Size="Small" ForeColor="Black" Text="台北市北投區東華街一段22巷8號"></asp:Label>
            <br />
            <asp:Label ID="Label4" runat="server" Text="電話:" ForeColor="#0033CC"></asp:Label>
            <asp:Label ID="Label9" runat="server" Font-Size="Small" ForeColor="Black" Text="02-28201255 "></asp:Label>
            <br />
            <asp:Label ID="Label10" runat="server" Text="傳真:" ForeColor="#0033CC"></asp:Label>
            <asp:Label ID="Label7" runat="server" Font-Size="Small" ForeColor="Black" Text="02-28201216"></asp:Label>
            <br />
            <asp:Label ID="Label8" runat="server" Text="電子郵件:" ForeColor="#0033CC"></asp:Label>
        </b>
        <asp:LinkButton ID="LinkButton5" runat="server" Font-Bold="True" Font-Size="Large">cpi.org@msa.hinet.net </asp:LinkButton>
        <br />

        <b>
            <asp:Label ID="Label11" runat="server" Text="網址:" ForeColor="#0033CC"></asp:Label>
        </b>

        <asp:LinkButton ID="LinkButton6" runat="server" Font-Bold="True" Font-Size="Large">www.cpi.org.tw</asp:LinkButton>

    </div>
</small>

