﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hoarding : System.Web.UI.UserControl
{
    String connString;
    OleDbConnection Con;
    protected void Page_Load(object sender, EventArgs e)
    {
        connString = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        if (Session["Username"] != null && Convert.ToInt16(Session["Pasw"]) == 1)
        {

            OleDbConnection conn = new OleDbConnection(connString);
            conn.Open();
            OleDbCommand cmd = new OleDbCommand("select * from UserTable Where  Username='" + Session["Username"].ToString() + "'", conn);
            OleDbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (dr["Rank"].ToString() != "")
                {
                    Button1.Visible = true;
                    Button2.Visible = true;
                }

            }
            dr.Dispose();
            conn.Close();
            conn.Dispose();
            cmd.Dispose();
        }
        else {
            Button1.Visible = false;
            Button2.Visible = false;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Session["index"] =7;
        Response.Redirect("Default.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Session["index"] = 8;
        Response.Redirect("Default.aspx");
    }
}