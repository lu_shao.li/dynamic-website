﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="hoarding.ascx.cs" Inherits="hoarding" %>
<style type="text/css">

    #postheader1 {
        width: 850px;
        height: 800px;
    }
        #postheader0 {
        width: 850px;
        height: 800px;
        margin-right: 0px;
    }
    </style>

<header>
    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Italic="True" Text="最新消息!!!" ForeColor="Red"></asp:Label>
    <asp:Button ID="Button1" runat="server"  Text="最新消息管理" Visible="False" OnClick="Button1_Click" />
    <br />
   
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True"  AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Act_Id" DataSourceID="AccessDataSource1" EmptyDataText="沒有資料錄可顯示。" ForeColor="#333333" GridLines="None" Width="830px" Height="313px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:TemplateField SortExpression="Datedata" HeaderText="發表日期">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Datedata") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <div align="center">
                    <asp:Label ID="Label1" runat="server" ForeColor="#3333FF" Text='<%# Bind("Datedata") %>' Width="100px"></asp:Label></div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="ActTitle" HeaderText="標題">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("ActTitle") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("ActTitle") %>' Width="400px"></asp:Label>
                </ItemTemplate>
                <ControlStyle Font-Bold="True" />
            </asp:TemplateField>
            <asp:HyperLinkField DataNavigateUrlFields="Act_Id" DataNavigateUrlFormatString="~/Content.aspx?id={0}" DataTextField="Summary" HeaderText="內容" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
       
    <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/db_ADO.mdb" SelectCommand="SELECT * FROM [Article] ORDER BY [Act_Id] DESC">
    </asp:AccessDataSource>
    <br />
    <br />

     </header>

    <header>
    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Italic="True" Text="活動專區!!!" ForeColor="Red"></asp:Label>
    <asp:Button ID="Button2" runat="server"  Text="活動管理" Visible="False" OnClick="Button2_Click" />
    <br />
    <asp:GridView ID="GridView2" runat="server" AllowPaging="True"  AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Act_Id" DataSourceID="AccessDataSource2" EmptyDataText="沒有資料錄可顯示。" GridLines="Horizontal" Width="800px">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <Columns>
            <asp:TemplateField SortExpression="Datedata" HeaderText="發表日期">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Datedata") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <div align="center">
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Datedata") %>' Width="90px"></asp:Label>
                       
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="活動名稱">
                <ItemTemplate>
                    <div align="center">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("Act_Id", "~/Activity.aspx?id={0}") %>' Text='<%# Eval("ActTitle") %>'></asp:HyperLink>
                        </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />
    </asp:GridView>
    <asp:AccessDataSource ID="AccessDataSource2" runat="server" DataFile="~/App_Data/db_ADO.mdb" SelectCommand="SELECT * FROM [Activity] ORDER BY [Act_Id] DESC"></asp:AccessDataSource>
        <br />
        <br />
    </header>
    <br />
    <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="#3366FF"></asp:Label>
    <br />
    <asp:Label ID="Label2" runat="server" ForeColor="#FF9900"></asp:Label>
    <br />


