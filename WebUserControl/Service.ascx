﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Service.ascx.cs" Inherits="WebUserControl_Service" %>
<section>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:normal"><span lang="EN-US" style="font-size:14.0pt;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-fareast"><span style="mso-spacerun:yes">&nbsp;</span></span></b><b style="mso-bidi-font-weight:
normal"><span lang="EN-US" style="font-size: 14.0pt; font-family: 新細明體, serif; mso-ascii-theme-font: minor-fareast; mso-fareast-font-family: 新細明體; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-fareast"><span id="ContentPlaceHolder1_Label12" style="color:Blue;font-size:17pt;font-weight:bold;">1. 儲油設備代行檢查</span>
                </span></b></p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:normal"><span style="font-size:14.0pt;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:
minor-fareast">〈<span lang="EN-US">1</span>〉 本學會為經濟部依據石油管理法「石油業儲油設備代行檢查機構設置管理辦法」指定之代行檢查機構〈詳附件：<a href="./Download/img-131105095407.pdf"><span lang="EN-US">10102007611</span>號函</a>及<a href="./Download/26117155371.pdf">第<span lang="EN-US">10102007610</span>號公告</a>〉<span lang="EN-US"><o:p></o:p></span></span></b></p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:normal"><span style="font-size:14.0pt;font-family:
&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-fareast">〈<span lang="EN-US">2</span>〉 依據石油管理法「石油業儲油設備設置管理規則」第二十七條規定「石油業者儲油設備，應委託經中央主管機關指定之代行檢杳機構實施外部及內部檢查並作成紀錄。油槽外部檢查應每二年實施一次。油槽新建完工達十年者，應即實施內部檢查，其後每五年應賞施一次。但經第一項代行檢查機構認定得延長其後每五年之檢查年限者，得檢具評估報告向直轄市、縣〈市〉主管機關申請延長一次；延長期限不得超過五年。」<span lang="EN-US"><o:p></o:p></span></span></b></p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:normal"><span style="font-size:14.0pt;font-family:
&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-fareast">〈<span lang="EN-US">3</span>〉 本學會目前己經手代行檢查儲油設備超過<span lang="EN-US">3,000</span>座以上，經驗豐富可靠。</span></b></p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:normal"><span style="font-size:18.0pt;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;
mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-fareast;
mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;
mso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-bidi-language:AR-SA">檢查實況照片：</span></b></p>
            <p class="MsoNormal">
                <img id="ContentPlaceHolder1_Image1" src="image/IMG_0078.JPG" style="height:267px;width:213px;" />
                <img id="ContentPlaceHolder1_Image2" src="image/IMG_0080.JPG" style="height:267px;width:213px;" />
                <img id="ContentPlaceHolder1_Image3" src="image/IMG_0093.JPG" style="height:267px;width:205px;" />
                <img id="ContentPlaceHolder1_Image4" src="image/IMG_0096_(2).JPG" style="height:267px;width:205px;" />
            </p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:
normal"><span lang="EN-US" style="font-size: 14.0pt; font-family: 新細明體, serif; mso-ascii-theme-font: minor-fareast; mso-fareast-font-family: 新細明體; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-fareast"><span id="ContentPlaceHolder1_Label13" style="color:Blue;font-size:17pt;font-weight:bold;">2. 加油站、加氣站、天然氣配氣站等設備檢查</span>
                </span></b>
            </p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:
normal"><span lang="EN-US" style="font-size: 14.0pt; font-family: 新細明體, serif; mso-ascii-theme-font: minor-fareast; mso-fareast-font-family: 新細明體; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-fareast"><span id="ContentPlaceHolder1_Label14" style="color:Blue;font-size:17pt;font-weight:bold;">3. 提供石油工業技術、地熱探勘、地質探勘、加油站經營及新建改建等專案研究。</span>
                </span></b>
            </p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:
normal"><span lang="EN-US" style="font-size: 14.0pt; font-family: 新細明體, serif; mso-ascii-theme-font: minor-fareast; mso-fareast-font-family: 新細明體; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-fareast"><span id="ContentPlaceHolder1_Label15" style="color:Blue;font-size:17pt;font-weight:bold;">4. 提供石油工業安全衛生及污染整治專案研究。 </span>
                </span></b>
            </p>
            <p class="MsoNormal">
                <b style="mso-bidi-font-weight:
normal"><span lang="EN-US" style="font-size: 14.0pt; font-family: 新細明體, serif; mso-ascii-theme-font: minor-fareast; mso-fareast-font-family: 新細明體; mso-fareast-theme-font: minor-fareast; mso-hansi-theme-font: minor-fareast"><span id="ContentPlaceHolder1_Label16" style="color:Blue;font-size:17pt;font-weight:bold;">5. 舉辦最新進石化技術、設備發表會。</span>
                </span></b>
            </p>
            <p class="MsoNormal">
                <span id="ContentPlaceHolder1_Label11" style="color:Red;font-size:Larger;font-weight:bold;font-style:normal;">※ 如需以上技術服務，敬請連絡我們</span>
            </p>
        </section>

