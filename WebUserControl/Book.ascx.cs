﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControl_Book : System.Web.UI.UserControl
{
    
     String connString ;
    OleDbConnection Con ;
    protected void Page_Load(object sender, EventArgs e)
    {
        connString = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        
        Session["Book1"] = "";
        Session["Book3"] = "";
        Session["Book2"] = "";
        Session["Mark"] = "";
        Session["Mark2"] = "";
        Session["Mark3"] = "";
        Session["Note"] = "";
        Session["Note2"] = "";
        Session["Note3"] = "";
        Check(1);
        Check(2);
        Check(3);
        Label4.Text = "";
       
        Label22.Text = "";




        if (Session["Username"] != null && Convert.ToInt16(Session["Pasw"]) == 1)
        {
            OleDbConnection Con = new OleDbConnection(connString);
            Con.Open();
            OleDbCommand cmd = new OleDbCommand("select * from UserTable Where  Username='" + Session["Username"].ToString() + "'", Con);
            OleDbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                if (dr["Rank"].ToString() != "")
                {

                    Button3.Visible = true;
                    FileUpload1.Visible = true;
                    Label2.Visible = true;
                    Label3.Visible = true;
                    TextBox1.Visible = true;


                    Label6.Visible = true;
                    Button4.Visible = true;
                    CheckBoxList4.Visible = true;

                    TextBox9.Visible = true;
                    TextBox8.Visible = true;
                    Label25.Visible = true;
                    Label26.Visible = true;
                    //學會圖書目錄
                    Label20.Visible = true;
                    Label21.Visible = true;
                    Label22.Visible = true;
                    Label23.Visible = true;
                    TextBox3.Visible = true;
                    FileUpload3.Visible = true;
                    CheckBoxList3.Visible = true;
                    Button11.Visible = true;
                    Button10.Visible = true;

                    TextBox6.Visible = true;
                    TextBox7.Visible = true;

                    Label27.Visible = true;
                    Label28.Visible = true;

                }
            }
            dr.Dispose();
            Con.Close();
            Con.Dispose();
            cmd.Dispose();
        }
        else {
            Button3.Visible = false;
            FileUpload1.Visible = false;
            Label2.Visible = false;
            Label3.Visible = false;
            TextBox1.Visible = false;


            Label6.Visible = false;
            Button4.Visible = false;
            CheckBoxList4.Visible = false;

            TextBox9.Visible = false;
            TextBox8.Visible = false;
            Label25.Visible = false;
            Label26.Visible = false;
            //學會圖書目錄
            Label20.Visible = false;
            Label21.Visible = false;
            Label22.Visible = false;
            Label23.Visible = false;
            TextBox3.Visible = false;
            FileUpload3.Visible = false;
            CheckBoxList3.Visible = false;
            Button11.Visible = false;
            Button10.Visible = false;

            TextBox6.Visible = false;
            TextBox7.Visible = false;

            Label27.Visible = false;
            Label28.Visible = false;
        }
    }
        private void Check(int id)
        {
            Con = new OleDbConnection(connString);
            Con.Open();
            OleDbCommand cmd0 = new OleDbCommand("select * from BookDisplay Where  B_ID=" + id + "", Con);
            OleDbDataReader dr0 = cmd0.ExecuteReader();
            int count = 0;
            while (dr0.Read())
            {
                Session["Book"+id] = dr0["URL"].ToString();
               
                if (++count == 1)
                    break;
            }
            dr0.Dispose();
            Con.Close();
            Con.Dispose();
            cmd0.Dispose();
        }
        protected void Upload(FileUpload myFileUpload)
        {
            Boolean fileAllow = false;
            String[] allowExtensions = { ".pdf" };
            String path = HttpContext.Current.Request.MapPath("~/Pdf/Book/");
            if (myFileUpload.HasFile)
            {
                String fileExtension = System.IO.Path.GetExtension(myFileUpload.FileName).ToLower();
                for (int i = 0; i < allowExtensions.Length; i++)
                {
                    if (fileExtension == allowExtensions[i])
                    {
                        fileAllow = true;
                    }

                }
                if (fileAllow)
                {
                    try
                    {

                        myFileUpload.SaveAs(path + myFileUpload.FileName);

                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {

                }

            }
        }
    protected void Button3_Click(object sender, EventArgs e)
    {
        String fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
        Boolean format = false;
        if (fileExtension == ".pdf")
        {
            format = true;
        }
        else
        {
            Label4.Text += "非PDF檔!";
        }
        if (FileUpload1.HasFile)
        {
            if (TextBox1.Text != "")
            {
                if (format)
                {
                    Con = new OleDbConnection(connString);
                    Con.Open();
                    OleDbCommand com = new OleDbCommand("Insert Into BookDisplay(Title,URL,Typ,Mark,Mark1)  Values ('" + TextBox1.Text + "','" + "Pdf/Book/" + FileUpload1.FileName + "','1','" + TextBox8.Text + "','" + TextBox9.Text + "')", Con);
                    com.ExecuteNonQuery();
                    Upload(FileUpload1);
                    Con.Close();
                    Con.Dispose();
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Label4.Text = "標題空白!";
            }
        }
        else
        {
            Label4.Text = "沒有檔案!";
        }
    }
    protected void Button4_Click(object sender, EventArgs e)
    {

        Con = new OleDbConnection(connString);
        Con.Open();
        OleDbCommand dcom = new OleDbCommand("Select * From BookDisplay Where B_ID=" + CheckBoxList4.SelectedValue, Con);
        OleDbDataReader drd = dcom.ExecuteReader();
        while (drd.Read())
        {
            Session["1DURL"] = drd["URL"].ToString();
        }
       
        dcom.Dispose();
        OleDbCommand com = new OleDbCommand("Delete From BookDisplay Where B_ID=" + CheckBoxList4.SelectedValue, Con);
        com.ExecuteNonQuery();
        Con.Close();
        com.Dispose();
        Con.Dispose();
        System.IO.FileInfo f = new System.IO.FileInfo(Server.MapPath(Session["1DURL"].ToString()));
        try
        {
            f.Delete();
        }
        catch (System.IO.IOException ex)
        {
            
        }
        Response.Redirect("Default.aspx");
    }
    protected void DropDownList5_SelectedIndexChanged(object sender, EventArgs e)
    {
        sel();
    }
    protected void Button10_Click(object sender, EventArgs e)
    {
        String fileExtension = System.IO.Path.GetExtension(FileUpload3.FileName).ToLower();
        Boolean format = false;
        if (fileExtension == ".pdf")
        {
            format = true;
        }
        else
        {
            Label22.Text += "非PDF檔!";
        }
        if (FileUpload3.HasFile)
        {
            if (TextBox3.Text != "")
            {
                if (format)
                {
                    Con = new OleDbConnection(connString);
                    Con.Open();
                    OleDbCommand com = new OleDbCommand("Insert Into BookDisplay(Title,URL,Typ,Mark,Mark1)  Values ('" + TextBox3.Text + "','" + "Pdf/Book/" + FileUpload3.FileName + "','2','" + TextBox6.Text + "','" + TextBox7.Text + "')", Con);
                    com.ExecuteNonQuery();
                    Upload(FileUpload3);
                    com.Dispose();
                    Con.Close();
                    Con.Dispose();
                    Response.Redirect("Default.aspx");
                }
            }
            else
            {
                Label22.Text = "標題空白!";
            }
        }
        else
        {
            Label22.Text = "沒有檔案!";
        }
    }
    protected void Button11_Click(object sender, EventArgs e)
    {
        
        Con = new OleDbConnection(connString);
        Con.Open();
        OleDbCommand dcom = new OleDbCommand("Select * From BookDisplay Where B_ID=" + CheckBoxList3.SelectedValue, Con);
       
        OleDbDataReader drd = dcom.ExecuteReader();
        while (drd.Read())
        {
            Session["DURL"] = drd["URL"].ToString();
        }
        dcom.Dispose();
        
        OleDbCommand com = new OleDbCommand("Delete From BookDisplay Where B_ID=" + CheckBoxList3.SelectedValue, Con);
        com.ExecuteNonQuery();
        Con.Close();
        com.Dispose();
        Con.Dispose();
       
        System.IO.FileInfo f = new System.IO.FileInfo(Server.MapPath(Session["DURL"].ToString()));
        try
        {
            f.Delete();
        }
        catch (System.IO.IOException ex)
        {
           
        }

        Response.Redirect("Default.aspx");
    }
    protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
    {
        sel2();
    }
    private void sel()
    {
        Label11.Text = "";
        Label12.Text = "";
        Con = new OleDbConnection(connString);
        Con.Open();
        OleDbCommand com = new OleDbCommand("Select * From BookDisplay where B_ID=" + DropDownList5.SelectedValue, Con);
        OleDbDataReader dr = com.ExecuteReader();

        while (dr.Read())
        {
            Session["Book1"] = dr["URL"].ToString();
            Label11.Text = dr["Mark"].ToString();
            Label12.Text = dr["Mark1"].ToString();
        }
        Con.Close();
        Con.Dispose();
    }
    private void sel2()
    {
        
        Label11.Text = "";
        Label12.Text = "";
       
        
        OleDbConnection conn = new OleDbConnection(connString);
        conn.Open();
        OleDbCommand cmd = new OleDbCommand("Select * From BookDisplay where B_ID=" + DropDownList4.SelectedValue, conn);
        OleDbDataReader dr = cmd.ExecuteReader();
        while (dr.Read())
        {
            Session["Book2"] = dr["URL"].ToString();
            Label31.Text = dr["Mark"].ToString();
            Label32.Text = dr["Mark1"].ToString();
        }
        dr.Dispose();
        cmd.Dispose();
        conn.Close();
        conn.Dispose();
    }
    protected void CheckBoxList3_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }
    protected void Button12_Click(object sender, EventArgs e)
    {
       
        Con = new OleDbConnection(connString);
        Con.Open();
        OleDbCommand dcom = new OleDbCommand("Select * From BookDisplay Where B_ID=" + CheckBoxList4.SelectedValue, Con);
        OleDbDataReader drd = dcom.ExecuteReader();
        while (drd.Read())
        {
            Session["DURL"] = drd["URL"].ToString();
        }
        Label3.Text = Session["DURL"].ToString();
       dcom.Dispose();
        OleDbCommand com = new OleDbCommand("Delete From BookDisplay Where B_ID=" + CheckBoxList4.SelectedValue, Con);
        com.ExecuteNonQuery();
        Con.Close();
        com.Dispose();
        Con.Dispose();
        Response.Redirect("Default.aspx");
    }
}