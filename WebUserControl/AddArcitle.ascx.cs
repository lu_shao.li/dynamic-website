﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControl_AddArcitle : System.Web.UI.UserControl
{
    String Time = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Username"] != null)
            LabelAuthor.Text = Session["Username"].ToString();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            if (TextBoxTitle.Text == "" || TextBoxS.Text == "" || TextBoxAt.Text == "")
            {
                Label3.Text = "尚有空格位填!";
            }
            else
            {
                int Mon = DateTime.Now.Month;
                int Year = DateTime.Now.Year;
                int day = DateTime.Now.Day;
                String dat = Year.ToString() + "-" + Mon.ToString() + "-" + day.ToString();
                int H = DateTime.Now.Hour;
                int M = DateTime.Now.Minute;
                Time = H.ToString() + ":" + M.ToString();
                LabelTime.Text = Time;
                LabelDate.Text = dat;
                Upload(FileUpload1);
                AccessDataSourceAddActicle.Insert();
                Session["index"] = 0;
                //Session["index"] = 7;
                Response.Write("<script language='javascript'>window.alert('發文成功!');</script>");
                Response.Write("<script language='javascript'>window.location.href='./Default.aspx';</script>");
                
               
            }
        }
        catch (Exception ex)
        {

        }
    }
        protected void Upload(FileUpload myFileUpload) {
            Boolean fileAllow = false;
            String[] allowExtensions = { ".pdf", ".doc", ".docx", ".ppt", ".pptx", ".zip", ".jpg", ".gif", ".png"};
            String path = HttpContext.Current.Request.MapPath("~/Pdf/Article/");
            if (myFileUpload.HasFile)
            {
                String fileExtension = System.IO.Path.GetExtension(myFileUpload.FileName).ToLower();
                for (int i = 0; i < allowExtensions.Length; i++)
                {
                    if (fileExtension == allowExtensions[i])
                    {
                        fileAllow = true;
                    }

                }
                if (fileAllow)
                {
                    try
                    {

                        Label5.Text = "Pdf/Article/" + myFileUpload.FileName;
                        Label6.Text = myFileUpload.FileName;

                        myFileUpload.SaveAs(path + myFileUpload.FileName);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
    }
