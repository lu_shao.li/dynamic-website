﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControl_AddActivity : System.Web.UI.UserControl
{
    String dd = "";
    string ConStr = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void Upload(FileUpload myFileUpload)
    {
        Boolean fileAllow = false;
        String[] allowExtensions = { ".pdf" };
        String path = HttpContext.Current.Request.MapPath("~/Pdf/Active/" + dd);
        
        if (myFileUpload.HasFile)
        {
            String fileExtension = System.IO.Path.GetExtension(myFileUpload.FileName).ToLower();
            for (int i = 0; i < allowExtensions.Length; i++)
            {
                if (fileExtension == allowExtensions[i])
                {
                    fileAllow = true;
                }

            }
            if (fileAllow)
            {
                try
                {
                    myFileUpload.SaveAs(path + myFileUpload.FileName);
                }
                catch (Exception ex)
                {
                }
            }
            else
            {

            }

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
        Boolean format = false;
        if (fileExtension == ".pdf")
        {
            format = true;
        }
        if (TextBox1.Text == "" || TextBox2.Text == "" || TextBoxTitle.Text == "")
        {
            Label3.Text = "上有欄位未填!";
        }
        else
        {

            if (FileUpload1.HasFile)
            {
                if (format)
                {
                    
                    int Mon = DateTime.Now.Month;
                    int Year = DateTime.Now.Year;
                    int day = DateTime.Now.Day;
                    int Min = DateTime.Now.Minute;
                    int Sec = DateTime.Now.Second;

                    String dat = Year.ToString() + "-" + Mon.ToString() + "-" + day.ToString();
                    dd = Year.ToString() + Mon.ToString() + day.ToString() + Min.ToString() + Sec.ToString();

                    OleDbConnection conn = new OleDbConnection(ConStr);
                    conn.Open();
                    String sTime = DropDownList1.SelectedItem.Text + ":" + DropDownList5.SelectedItem.Text;
                    String eTime = DropDownList2.SelectedItem.Text + ":" + DropDownList6.SelectedItem.Text;
                    Upload(FileUpload1);
                    OleDbCommand cmd = new OleDbCommand("Insert Into Activity(ActTitle,ActDateStart,ActDateEnd,Datedata,StartTime,EndTime,PdfURI,isPass" +
                        ") Values('" + TextBoxTitle.Text + "','" + TextBox1.Text + "','" + TextBox2.Text + "','" + dat + "','" + sTime + "','" + eTime + "','" + "Pdf/Active/" + dd + FileUpload1.FileName + "','0')", conn);

                    cmd.ExecuteNonQuery();

                    cmd.Dispose();
                    conn.Close();
                    conn.Dispose();
                    Session["index"] = 0;
                    Response.Write("<script language='javascript'>window.alert('新增成功!');</script>");
                    Response.Write("<script language='javascript'>window.location.href='./Default.aspx';</script>");
                    
                
                   
                }
                else
                {
                    Label3.Text = "上傳格式並非PDF檔!";
                }

            }
            else
            {
                Label3.Text = "活動內容沒有檔案!!";
            }

        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 0;
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 1;
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Calendar2_SelectionChanged(object sender, EventArgs e)
    {
        String data = "";
        data += Calendar2.SelectedDate.Year.ToString() + "-" + Calendar2.SelectedDate.Month.ToString() + "-" + Calendar2.SelectedDate.Day.ToString();
        TextBox1.Text = data;
    }
    protected void Calendar4_SelectionChanged(object sender, EventArgs e)
    {
        String data = "";
        data += Calendar4.SelectedDate.Year.ToString() + "-" + Calendar4.SelectedDate.Month.ToString() + "-" + Calendar4.SelectedDate.Day.ToString();
        TextBox2.Text = data;
    }
}