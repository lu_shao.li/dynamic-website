﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControl_Information : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        String uploadPath = Server.MapPath("~/image/photos/imf");
        DirectoryInfo dirinfo = new DirectoryInfo(uploadPath);
        DataList1.DataSource = dirinfo.GetFiles();
        DataList1.DataBind();
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        sel();
    }
    private void sel()
    {
        string ConStr = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        OleDbConnection Con = new OleDbConnection(ConStr);
        Con.Open();
        OleDbCommand cmd = new OleDbCommand("select * from Activity ", Con);
        OleDbCommand cmd1 = new OleDbCommand("update Activity set isPass='1' where ActDateEnd='" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "' and EndTime='" + DateTime.Now.Hour + ":" + DateTime.Now.Minute + "'", Con);
        OleDbDataReader dr = cmd.ExecuteReader();

        while (dr.Read())
        {
            if (dr["ActDateEnd"].ToString().Equals(DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day))
            {
                if (dr["EndTime"].ToString().Equals(DateTime.Now.Hour + ":" + DateTime.Now.Minute))
                {

                    dr.Dispose();
                    cmd1.ExecuteNonQuery();
                    break;

                }
            }
        }

        cmd1.Dispose();
        cmd.Dispose();
        Con.Close();
        Con.Dispose();
    }
}