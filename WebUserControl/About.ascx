﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="About.ascx.cs" Inherits="WebUserControl_About" %>
<header id="postheader">
    <style type="text/css">

    #postheader1 {
        width: 500px;
    }
        #postheader0 {
        width: 500px;
    }
    </style>
    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">【學會簡介】</asp:LinkButton>
    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">【理事長的話】</asp:LinkButton>
    <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click">【本會章程】</asp:LinkButton>
    <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click">【理、監事暨職員名錄】</asp:LinkButton>
    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton5_Click">【學會組織圖】</asp:LinkButton>
   

    </header>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:MultiView ID="MultiView2" runat="server" ActiveViewIndex="0">
            <asp:View ID="View3" runat="server">
                <section>
                    <div align="center">
                        <h1>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" ForeColor="#0066FF" Text="成立50週年暨2012年年會歷史的回顧影片"></asp:Label>
                        </h1>
                        <iframe id="I1" class="youtube-player" frameborder="0" height="385" name="I1" src="http://www.youtube.com/embed/EHQQQoszXUY" type="text/html" width="640"></iframe>
                    </div>
                </section>
            </asp:View>
            <asp:View ID="View4" runat="server">
                <section>
                    <div align="center">
                        <h1>
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#0066FF" Text="組織架構圖"></asp:Label>
                        </h1>
                    </div>
                    <div align="center">
                        <p>
                            <asp:Image ID="Image1" runat="server" Height="350px" ImageUrl="~/image/Org.jpg" Width="643px" />
                        </p>
                    </div>
                </section>
            </asp:View>
            <asp:View ID="View5" runat="server">
                <section>
                    <div align="center">
                        <h1>
                            <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="#0066FF" Text="組織章程"></asp:Label>
                        </h1>
                    <br />
                        <object border="0" classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" height="700" width="860">
                        <param name="_Version" value="65539" />
                        <param name="_ExtentX" value="20108" />
                        <param name="_ExtentY" value="10866" />
                        <param name="_StockProps" value="0" />
                        <param name="SRC" value="Pdf/Org.pdf" />
                            <object data="Pdf/Org.pdf" height="700" type="application/pdf" width="860">
                            </object>
                        </object>
                    </div>
                </section>
            </asp:View>
            <asp:View ID="View6" runat="server">
                <section>
                    <div align="center">
                        <asp:Label ID="Label4" runat="server" Font-Bold="True" ForeColor="#0066FF" Text="理、監事暨職員名錄"></asp:Label>
                    <br />
                        <object border="0" classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" height="700" width="860">
                        <param name="_Version" value="65539" />
                        <param name="_ExtentX" value="20108" />
                        <param name="_ExtentY" value="10866" />
                        <param name="_StockProps" value="0" />
                        <param name="SRC" value="Pdf/31.pdf" />
                            <object data="Pdf/31.pdf" height="700" type="application/pdf" width="860">
                            </object>
                        </object>
                    </div>
                </section>
            </asp:View>
            <asp:View ID="View7" runat="server">
                <section>
                    <asp:Image ID="Image2" runat="server" Height="286px" ImageUrl="~/image/朱董事長2吋彩色照片-新.jpg" Width="252px" />
                    <span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">
                    <h1><span lang="EN-US" style="font-size:14.0pt;
line-height:125%"><span style="mso-spacerun:yes">
                        <asp:Label ID="Label15" runat="server" Font-Bold="True" ForeColor="#3333FF" Text="理事長的話"></asp:Label>
                        </span></span></h1>
                    </span>
                    <p class="MsoNormal">
                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">本會是民國</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">51 </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">年</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">7</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">月</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">7</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">日由金開英先生發起籌備，同年</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%">10</span><span style="font-size:14.0pt;line-height:125%;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">月</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">28</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">日正式成立，迄今逾</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%">51</span><span style="font-size:14.0pt;line-height:125%;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">年，是一個歷史悠久，會員眾多，對國家經濟發展、對我國的石油工業，有實質貢獻的民間、學術團體。學會當初成立的宗旨在於：</span><span style="font-size:14.0pt;line-height:125%"> </span>
                        <p class="MsoNormal">
                            <span lang="EN-US" style="font-size:14.0pt;line-height:125%">1. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">鼓勵石油學術交流，特別是與美國及大陸石油公司，在探勘、煉製、天然氣、油品貿易等方面的技術交流，迄今華人石油學會成立已逾<span lang="EN-US" style="font-size:14.0pt;line-height:
125%">30</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;
mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
mso-hansi-theme-font:minor-latin">年，每年仍然繼續積極的辦裡各樣的交流活動。</span></span><p class="MsoNormal">
                                <span lang="EN-US" style="font-size:14.0pt;
line-height:125%">2. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">推廣石油產品應用</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%"></span><p class="MsoNormal">
                                    <span lang="EN-US" style="font-size:14.0pt;line-height:125%">3. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">提昇石油產業科技水準</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%"></span><p class="MsoNormal">
                                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%">4. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">促進國家整體石油工業建設</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%"></span><p class="MsoNormal">
                                            <span lang="EN-US" style="font-size:14.0pt;
line-height:125%">5. </span><span style="font-size:14.0pt;line-height:125%;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">增近民眾福祉</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"></span><p class="MsoNormal">
                                                <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp; &nbsp;</span></span><span style="font-size:
14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">過去我們辦理多次專題研討會並參與亞洲石化會議、全球石油</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">/</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">石化科技研討會及與國內與能源有關之學術論壇</span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:
minor-fareast">，</span><span style="font-size:14.0pt;line-height:125%;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">以及承辦政府機關各項調查服務專案；油槽檢測、</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">LNG</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">冷凍槽操作手冊等</span><span style="font-size:14.0pt;line-height:125%;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-fareast">，</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">並新增教育訓練服務</span><span style="font-size:14.0pt;line-height:125%;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:minor-fareast">，非常受到會員的認同，效果良好。</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"></span><p class="MsoNormal">
                                                    <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">未來學會努力的方向：</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"></span><p class="MsoNormal">
                                                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%">1. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">擴大會員服務</span><span lang="EN-US" style="font-size:14.0pt;line-height:
125%"></span><p class="MsoNormal">
                                                            <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>a. </span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">教育訓練方面，要擴大辦理，並且針對會員實際的需要辦理。</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"></span><p class="MsoNormal">
                                                                <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp; &nbsp;</span>b. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">專業咨詢或現場服務</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%"></span><p class="MsoNormal">
                                                                    <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>c. </span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">擴大在技術評估、新科技引進、海外投資、購併，商務談判等</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">服務工作。</span></span><p class="MsoNormal">
                                                                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%">2. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">積極參與國內、外的學術活動</span><p class="MsoNormal">
                                                                            <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>a. </span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">主辦、協辦、參與各項國內、外學術活動、研討會、論文發表</span><p class="MsoNormal">
                                                                                <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">等</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:
minor-fareast;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-theme-font:minor-fareast">。</span><p class="MsoNormal">
                                                                                    <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>b. </span><span style="font-size:14.0pt;line-height:125%;font-family:
&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
Calibri;mso-hansi-theme-font:minor-latin">專題研究與能源、石化高質化、工安環保、節能減碳、效率提</span><p class="MsoNormal">
                                                                                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;
mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:
新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
mso-hansi-theme-font:minor-latin">昇、經營管理等有關之議題</span><span style="font-size:
14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:minor-fareast;
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-theme-font:
minor-fareast">。</span><p class="MsoNormal">
                                                                                            <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>c. </span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">出版學術、技術、市場動態、新科技、最新商情等專刊</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:
minor-fareast;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-theme-font:minor-fareast">。</span><p class="MsoNormal">
                                                                                                <span lang="EN-US" style="font-size:14.0pt;line-height:125%">3. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">協助政府機關</span><p class="MsoNormal">
                                                                                                    <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>a. </span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">承攬政府機關，各項檢查、評核、研究、宣導等有關之業務。</span><p class="MsoNormal">
                                                                                                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>b. </span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">對政府重大政策，提供專業性建言</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:
minor-fareast;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-theme-font:minor-fareast">。</span><p class="MsoNormal">
                                                                                                            <span lang="EN-US" style="font-size:14.0pt;line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;</span>c. </span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">參加政府各項重大科研及發展專案計畫</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-theme-font:
minor-fareast;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-theme-font:minor-fareast">。</span><p class="MsoNormal">
                                                                                                                <span lang="EN-US" style="font-size:14.0pt;line-height:125%">4. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">對學會內部建立學會更健全的財務基礎，厚植能量，提供更多元、更專業、更有效的服務。</span><p class="MsoNormal">
                                                                                                                    <span lang="EN-US" style="font-size:14.0pt;
line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label14" runat="server" Font-Bold="True" ForeColor="#3333FF" Text="感言"></asp:Label>
                                                                                                                    </span></span>
                                                                                                                    <p class="MsoNormal">
                                                                                                                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%">1. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">頁岩氣的開發，創造了新的能源革命，影響範圍，不僅是能源供</span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">應、原油價格，石化發展，產生巨大的變化，同時也帶動了全球政治、經濟、投資等等的變革，在這一波的變化浪潮之中，台灣身處劣勢，如果不採取積極作為，後果不堪設想，所以如何判斷情勢，積極作為，應為當務之急。</span><p class="MsoNormal">
                                                                                                                            <span lang="EN-US" style="font-size:14.0pt;line-height:125%">2. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">大陸煉油設備擴充迅速，同時世界經濟發展趨緩，亞洲的油品供</span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">應勢必過剩，加上現在政府及民間社會，對台灣石化工業的冷陌及敵視，這是未來發展石油</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">/</span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">石化工業最大的隱憂與瓶頸。</span><p class="MsoNormal">
                                                                                                                                <span lang="EN-US" style="font-size:14.0pt;line-height:125%">3. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">跨國企業組織及國際化發展，已經超越國界的範躊，形成新的世</span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">界經濟霸權，從原料掌控、市場、價格、貿易、規範制定等等，無所不用其極的伸入世界每個角落，弱肉強食，無一倖免，所以我們更應該積極參與，這是唯一的對策，沒有別的方法。</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"></span><p class="MsoNormal">
                                                                                                                                    <span lang="EN-US" style="font-size:14.0pt;line-height:125%">4. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">沒有任何一個獨立的國家，可以放棄能源與石化工業，放棄了，</span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">就等於成為世界經濟霸權經濟的殖民地，其後果不僅要付出更高昂的成本代價，也同時限制了經濟與文明發展，包括台灣在內。</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"></span><p class="MsoNormal">
                                                                                                                                        <span lang="EN-US" style="font-size:14.0pt;line-height:125%">5. </span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">最近我參加</span><span lang="EN-US" style="font-size:14.0pt;line-height:
125%">CAPA</span><span style="font-size:14.0pt;line-height:125%;font-family:
&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;
mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
Calibri;mso-hansi-theme-font:minor-latin">董事會，回顧過去的</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">30</span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">年，大陸的石油工業從非常小的規模開始發展，已經已成為世界能源的翹楚，未來</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%">30</span><span style="font-size:14.0pt;
line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;
mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:
minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">年，</span><span style="font-size:14.0pt;line-height:125%"> <span lang="EN-US">CAPA</span></span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">思索要如何成為世界能源真正的領袖。</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%"></span><p class="MsoNormal">
                                                                                                                                            <span lang="EN-US" style="font-size:14.0pt;
line-height:125%"><span style="mso-spacerun:yes">&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span style="font-size:14.0pt;line-height:125%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin">中國石油學會過去</span><span lang="EN-US" style="font-size:14.0pt;
line-height:125%">50</span><span style="font-size:14.0pt;line-height:125%;
font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin">年來，帶動了台灣經濟的發展，未來我們要以更積極的角色，貢獻社會、貢獻國家，這是我們現在應該深思及採取行動的關鍵時刻與使命。</span><span lang="EN-US" style="font-size:14.0pt;line-height:125%"></span></p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                        <p>
                                                                                                                                        </p>
                                                                                                                                    </p>
                                                                                                                                </p>
                                                                                                                            </p>
                                                                                                                        </p>
                                                                                                                    </p>
                                                                                                                </p>
                                                                                                            </p>
                                                                                                        </p>
                                                                                                    </p>
                                                                                                </p>
                                                                                            </p>
                                                                                        </p>
                                                                                    </p>
                                                                                </p>
                                                                            </p>
                                                                        </p>
                                                                    </p>
                                                                </p>
                                                            </p>
                                                        </p>
                                                    </p>
                                                </p>
                                            </p>
                                        </p>
                                    </p>
                                </p>
                            </p>
                        </p>
                    </p>
                </section>
            </asp:View>
            <asp:View ID="View8" runat="server">
                <section>
                    <div align="center">
                        <h1>
                            <asp:Label ID="Label5" runat="server" Font-Bold="True" ForeColor="#0066FF" Text="煤的新世紀"></asp:Label>
                        </h1>
                        <object border="0" classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" style="height: 603px" width="860">
                        <param name="_Version" value="65539" />
                        <param name="_ExtentX" value="20108" />
                        <param name="_ExtentY" value="10866" />
                        <param name="_StockProps" value="0" />
                        <param name="SRC" value="Pdf/煤的新世紀.pdf" />
                            <object data="http://localhost:52352/Pdf/煤的新世紀.pdf" style="height: 647px" type="application/pdf" width="860">
                            </object>
                        </object>
                    </div>
                </section>
            </asp:View>
            <asp:View ID="View1" runat="server">
                <section>
                    <div align="center">
                        <h1>
                            <asp:Label ID="Label6" runat="server" Font-Bold="True" ForeColor="#0066FF" Text="New Century of Coal"></asp:Label>
                        </h1>
                        <object border="0" classid="clsid:CA8A9780-280D-11CF-A24D-444553540000" style="height: 603px" width="860">
                        <param name="_Version" value="65539" />
                        <param name="_ExtentX" value="20108" />
                        <param name="_ExtentY" value="10866" />
                        <param name="_StockProps" value="0" />
                        <param name="SRC" value="Pdf/New_Century_of_Coal.pdf" />
                            <object data="http://localhost:52352/Pdf/New_Century_of_Coal.pdf" style="height: 647px" type="application/pdf" width="860">
                            </object>
                        </object>
                    </div>
                </section>
            </asp:View>
            <asp:View ID="View9" runat="server">
                <section>
                    <header>
                        <font color="purple" face="標楷體" size="6">關於中國石油學會<br /> <b><font color="blue" face="標楷體" size="4">&nbsp;&nbsp;&nbsp; 係於民國五十一年(1962年) 十月由國內石油專家、學者、教授、政府有關機構及中油公司主持人等共同發起成立，其宗旨在鼓勵石油學術交流，推廣石油產品應用，並提昇石油產業科技水準，以促進國家整體石油工業建設，增益民眾福祉。</font></b></font></header>
                    <p class="MsoNormal">
                        <b style="mso-bidi-font-weight:normal"><span style="font-size:16.0pt;line-height:
150%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:Calibri;mso-ascii-theme-font:
minor-latin;mso-fareast-font-family:新細明體;mso-fareast-theme-font:minor-fareast;
mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin"><font color="blue">組織概況</font></span></b><br>
                        <div>
                            <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Large" Text="Label"></asp:Label>
                        </div>
                        <p class="MsoNormal">
                            <b style="mso-bidi-font-weight:normal"><span style="font-size:16.0pt;line-height:150%;font-family:&quot;新細明體&quot;,&quot;serif&quot;;mso-ascii-font-family:
Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:新細明體;
mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:
minor-latin"><font color="blue">會務簡介</font></span><span lang="EN-US" style="font-size:16.0pt;line-height:150%">
                        <br />
                            <div>
                                <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Size="Large" Text="Label"></asp:Label>
                            </div>
                            </span></b>
                            <p>
                            </p>
                            <p>
                            </p>
                            <p>
                            </p>
                            <p>
                            </p>
                        </p>
                        </br>
                    </p>
                </section>
            </asp:View>
        </asp:MultiView>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="LinkButton1" />
        <asp:AsyncPostBackTrigger ControlID="LinkButton2" />
        <asp:AsyncPostBackTrigger ControlID="LinkButton3" />
        <asp:AsyncPostBackTrigger ControlID="LinkButton4" />
        <asp:AsyncPostBackTrigger ControlID="LinkButton5" />
    </Triggers>
</asp:UpdatePanel>


