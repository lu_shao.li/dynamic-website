﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebUserControl_About : System.Web.UI.UserControl
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = Convert.ToInt16(Session["A_i"]);
        Label7.Text = "&nbsp; &nbsp; 本學會主要會務為提供石油學術及技術以及法規專業諮詢與研究， 重點工作如下：<BR><BR>" +
" 一. 為厚植石油、石化工業能力，本學會年會均配合國內外產業發展趨勢擬定主題，供會員進行意見交流與研討，年會中亦頒發石油事業獎座及石油技術獎章，並辦理論文徵文競賽，每年均有豐碩成果。<BR><BR>" +
" 二. 主辦或協辦各種石油科技研討會、座談會、論壇及參與國際石油組織活動，以及兩岸交流。<BR><BR>" +
" 三. 參加或主辦「全球華人石油及石化科技研討會」，以加強業界華人聯繫與資訊交流。<BR><BR>" +
" 四.為學術研究機構及政府部門提供技術服務及專業諮詢，如石油類及氣體燃料類之能源管理法規研訂等。<BR><BR>" +
" 五. 為政府有關部門辦理石油輸儲設備、汽車加油(氣)站及天然氣配氣站等代行檢查業務。<BR><BR>" +
" 六.辦理石油產業相關技術訓練教育，如「VOC處理技術」、「蒸餾工廠操作實務」、「加油站經營管理」、「施工安全管理」等等。<BR><BR>" +
" 七. 石化工業安全衛生及環境保護教學及推行，以及汙染整治技術之研究。<BR><BR>" +
" 八. 油料火災消防技術研究與訓練。<BR><BR>" +
" 九. 石油專業技術知識叢書之編輯與出版，並定期發行「石油季刊」。<BR><BR>" +
" 十. 探討新能源之開發，以及「節能減碳」之研究與推行。<BR><BR>";
        Label8.Text = "  &nbsp; &nbsp;中國石油學會目前有四十二個團體會員，主要分布於石油業、石化業、瓦斯業、學術機構、工程顧問公司、航運與交通運輸業等；另有個人會員五百餘人，多為石油科技、生產、營運及管理專才。<BR>"+
    " &nbsp; &nbsp;本學會設有技術服務部及行政部門，並另設技術委員會，下設探採組、煉製組、營運組、石油產品組、石化工業組、天然氣及液化天然氣組、 工安環保組、綠能科技組等八組，分別推動有關會務。";

    }
    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
       
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 4;
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 7;
    }
    protected void LinkButton6_Click1(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 0;
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 2;
    }
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 1;
    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        MultiView2.ActiveViewIndex = 3;
    }
    
}