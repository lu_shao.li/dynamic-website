﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UPWebForm : System.Web.UI.Page
{
    String connString;
    OleDbConnection Con;
    protected void Page_Load(object sender, EventArgs e)
    {
        connString = WebConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Upload(FileUpload1);
    }
    protected void Upload(FileUpload myFileUpload)
    {
        Boolean fileAllow = false;
        // String[] allowExtensions = { ".jpg", ".gif", ".png" ,".png" };
        String[] allowExtensions = { ".pdf", ".doc", ".docx", ".ppt", ".pptx", ".zip", ".jpg", ".gif", ".png" };
        String path = HttpContext.Current.Request.MapPath("~/Pdf/Article/");
        if (myFileUpload.HasFile)
        {
            String fileExtension = System.IO.Path.GetExtension(myFileUpload.FileName).ToLower();
            for (int i = 0; i < allowExtensions.Length; i++)
            {
                if (fileExtension == allowExtensions[i])
                {
                    fileAllow = true;
                }

            }
            if (fileAllow)
            {
                try
                {


                    myFileUpload.SaveAs(path + myFileUpload.FileName);

                    OleDbConnection Con = new OleDbConnection(connString);
                    Con.Open();
                   
                  //  Upload(FileUpload1);
                    OleDbCommand cmd = new OleDbCommand("Update Article Set PdfURI='Pdf/Article/" + FileUpload1.FileName + "',PdfFileName='" + myFileUpload.FileName + "' where Act_Id=" + Request["id"], Con);
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    Con.Dispose();
                    cmd.Dispose();
                    Response.Redirect("Default.aspx");

                }
                catch (Exception ex)
                {
                    Label1.Text = ex.ToString();
                }
            }


        }
    }
}